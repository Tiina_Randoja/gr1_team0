﻿using System;

namespace NAmeAndCity
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World");
        }

        public static void PrintHello(string message)
        {
             Console.WriteLine(message);
        }

        public static string NameAndCity(string name, string city)
        {
            return $"{name} lives in {city}";
        }
    }
}
